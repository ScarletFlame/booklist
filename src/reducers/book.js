/*
Редьюсер для книг
 */
import getUuid from 'uuid-by-string'

const initialState = {
  books: []
}

/**
 * Сохраняем книги в localStorage
 * @param books
 */
const saveBooks = books => {
  localStorage.setItem('books', JSON.stringify(books))
}

export default (state = initialState, action) => {
  switch (action.type) {
    // Добавление книги
    case 'ADD_BOOK': {
      const { book } = action.payload
      book.id = getUuid(book.title + book.authors.join(''))

      const books = state.books.slice()
      books.push(book)

      sort(books)
      saveBooks(books)

      return {
        ...state,
        books
      }
    }

    // Загрузка книг
    case 'LOAD_BOOKS': {
      const { books } = action.payload

      return {
        ...state,
        books
      }
    }

    // Удаление книг
    case 'DELETE_BOOK': {
      const { uuid } = action.payload
      const books = state.books.slice()

      const index = books.findIndex(book => book.id === uuid)

      if (index !== -1) {
        books.splice(index, 1)
        sort(books)
        saveBooks(books)
        return {
          ...state,
          books
        }
      }

      return state
    }

    // Сохранение книг
    case 'SAVE_BOOK': {
      const { book } = action.payload
      const books = state.books.slice()

      const index = books.findIndex(b => book.id === b.id)

      if (index !== -1) {
        books[index] = book
        saveBooks(books)
        return {
          ...state,
          books
        }
      }

      return state
    }

    // Сортируем книги
    case 'SORT_BOOKS': {
      const { field, dir } = action.payload
      const books = state.books.slice()

      localStorage.setItem('field', field)
      localStorage.setItem('dir', dir)

      sort(books)
      saveBooks(books)

      return {
        ...state,
        books
      }
    }

    default:
      return state
  }
}

// Сортировка по заданному полю и направлению
const sort = books => {
  const field = localStorage.getItem('field')
  const dir = parseInt(localStorage.getItem('dir'))

  books.sort((a, b) => {
    if (a[field] > b[field]) {
      return dir
    }
    if (a[field] < b[field]) {
      return -1 * dir
    }
    return 0
  })
}
