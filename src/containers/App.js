/**
 * Компонент приложения
 */
import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import PropTypes from 'prop-types'
import Home from './Home'
import Header from '../components/Header'
import BookForm from './BookForm'
import Toolbar from '../components/Toolbar'
import { connect } from 'react-redux'
import { loadBooks } from '../actions/book'

class App extends Component {
  static propTypes = {
    loadBooks: PropTypes.func
  }

  componentDidMount() {
    // При загрузке приложения загружаем книги из localStorage
    const books = localStorage.getItem('books')
    if (books) {
      this.props.loadBooks(JSON.parse(books))
    }
  }

  render() {
    return (
      <div className="layout">
        <Header />
        <Toolbar />
        <Switch>
          <Route exact path={'/'} component={Home} />
          <Route exact path={'/add'} component={BookForm} />
          <Route exact path={'/edit/:uuid'} component={BookForm} />
        </Switch>
      </div>
    )
  }
}

export default connect(
  null,
  { loadBooks }
)(App)
