/*
Компонент списка книг/главная страница
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Book from '../components/Book'
import { sortBooks } from '../actions/book'

class Home extends Component {
  static propTypes = {
    books: PropTypes.array,
    sortBooks: PropTypes.func
  }

  constructor(props) {
    super(props)

    this.state = {
      books: [],
      dir: 1,
      field: null
    }

    this.setBooks = this.setBooks.bind(this)
    this.sort = this.sort.bind(this)
  }

  componentDidMount() {
    this.setBooks()

    // Получаем сортировку
    const field = localStorage.getItem('field')
    const dir = localStorage.getItem('dir')

    const state = {}
    if (field) {
      state.field = field
    }
    if (dir) {
      state.dir = parseInt(dir)
    }

    this.setState(state)
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps.books) !== JSON.stringify(this.props.books)) {
      // Загружаем книги
      this.setBooks()
    }
  }

  /**
   * Загружаем книги
   */
  setBooks() {
    const { books } = this.props

    this.setState({
      books
    })
  }

  /**
   * Сортировка по заданному полю
   * @param field
   */
  sort(field) {
    const dir = this.state.dir * -1
    this.setState({
      field,
      dir
    })
    this.props.sortBooks(field, dir)
  }

  /**
   * Добавляем классы ссылкам сортировки
   * @param curField - текущее поле
   * @param field - поле сортировки
   * @param dir - направление сортировки
   * @returns {string}
   */
  addClasses(curField, field, dir) {
    let className = ''
    if (field && field === curField) {
      className += ' sort__link_active'

      if (dir) {
        className += ` sort__link_${dir > 0 ? 'asc' : 'desc'}`
      }
    }

    return className
  }

  render() {
    const { books, field, dir } = this.state
    return (
      <div className="center">
        {books.length > 0 && (
          <div className="sort">
            <div className="sort__title">Сортировать: </div>
            <a
              className={`sort__link${this.addClasses('title', field, dir)}`}
              onClick={e => this.sort('title')}
            >
              Заголовок
            </a>
            <a
              className={`sort__link${this.addClasses('year', field, dir)}`}
              onClick={e => this.sort('year')}
            >
              Год публикации
            </a>
          </div>
        )}
        <div className="books">
          {books.map((book, i) => (
            <Book {...book} key={i} />
          ))}
          {books.length === 0 && (
            <div className="books__empty">Список книг пуст</div>
          )}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    books: state.book.books
  }
}

export default connect(
  mapStateToProps,
  { sortBooks }
)(Home)
