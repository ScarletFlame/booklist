/*
Компонент формы
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Field } from 'react-final-form'
import SuggestAdapter from '../components/SuggestAdapter'
import authorList from '../../static/authors'
import publisherList from '../../static/publishers'
import ISBN from 'isbnjs'
import { connect } from 'react-redux'
import { addBook, saveBook } from '../actions/book'
import history from '../history'

class BookForm extends Component {
  static propTypes = {
    addBook: PropTypes.func,
    saveBook: PropTypes.func,
    bookCount: PropTypes.number,
    match: PropTypes.shape({
      params: PropTypes.object,
      path: PropTypes.string
    }),
    book: PropTypes.shape({
      title: PropTypes.string,
      authors: PropTypes.arrayOf(PropTypes.string),
      pageCount: PropTypes.string,
      publisher: PropTypes.string,
      year: PropTypes.string,
      date: PropTypes.string,
      isbn: PropTypes.string,
      coverData: PropTypes.string,
      id: PropTypes.string
    })
  }

  constructor(props) {
    super(props)

    this.state = {
      authors: [],
      newAuthor: '',
      authorError: [],
      coverData: null,
      showMessage: false
    }

    this.onSubmit = this.onSubmit.bind(this)
    this.addAuthor = this.addAuthor.bind(this)
    this.removeAuthor = this.removeAuthor.bind(this)
    this.onChangeAuthor = this.onChangeAuthor.bind(this)
    this.selectImage = this.selectImage.bind(this)
    this.removeImage = this.removeImage.bind(this)
    this.setInitial = this.setInitial.bind(this)
  }

  /**
   * Подставляем авторов и картинку
   */
  setInitial() {
    const { book } = this.props
    if (book) {
      this.setState({
        authors: book.authors,
        coverData: book.coverData
      })
    }
  }

  componentDidMount() {
    this.setInitial()
  }

  componentDidUpdate(prevProps) {
    // При изменении количества книг показываем плашку, что книга сохранена
    if (
      prevProps.bookCount !== 0 &&
      prevProps.bookCount < this.props.bookCount
    ) {
      this.setState({
        showMessage: true
      })

      // Убираем плашку через секунду
      setTimeout(() => {
        this.setState({
          showMessage: false
        })
      }, 1000)
    }

    // Подсталяем в поля значения
    if (!prevProps.book && this.props.book) {
      this.setInitial()
    }

    // Если был переход с редактирования на добавление, то очищаем поля авторов и картинки
    if (this.props.match.path !== prevProps.match.path) {
      this.setState({
        authors: [],
        coverData: null
      })
    }
  }

  /**
   * Изменение  автора
   * @param value - имя и фамилия
   */
  onChangeAuthor(value) {
    this.setState({
      newAuthor: value,
      authorError: []
    })
  }

  /**
   * Добавление автора
   */
  addAuthor() {
    const { newAuthor, authors } = this.state
    const error = []

    if (newAuthor !== '') {
      // Удаляем множественные пробелы на всякий случай
      const parts = newAuthor.replace(/\s\s+/g, ' ').split(' ')
      // Считаем, что последнее слово - фамилия, все остальное - имя
      const lastName = parts.pop()
      const firstName = parts.join(' ')

      // Формируем имя и фамилию
      const author = `${firstName} ${lastName}`

      // Валидируем автоора
      if (firstName === '' || lastName === '') {
        error.push('Введите имя и фамилию автора')
      }
      if (!this.validateLength(firstName, 20)) {
        error.push('Имя автора не должно быть длиннее 20 символов')
      }
      if (!this.validateLength(lastName, 20)) {
        error.push('Фамилия автора не должна быть длиннее 20 символов')
      }

      if (error.length === 0) {
        if (authors.indexOf(author) === -1) {
          // Добавляем автора
          const newAuthors = authors.slice()
          newAuthors.push(author)
          this.setState({
            authors: newAuthors,
            newAuthor: '',
            authorError: []
          })
        } else {
          error.push('Такой автор уже есть')
        }
      }
    } else {
      error.push('Введите имя и фамилию автора')
    }

    if (error.length) {
      // Добавляем ошибки при валидации автора
      this.setState({
        authorError: error
      })
    }
  }

  /**
   * Удаляем автора
   * @param author - имя автора
   */
  removeAuthor(author) {
    const { authors } = this.state
    const index = authors.indexOf(author)
    authors.splice(index, 1)
    this.setState({
      author
    })
  }

  /**
   * Загружаем картинку
   * @param e
   */
  selectImage(e) {
    const files = Array.from(e.target.files)

    const reader = new FileReader()
    reader.readAsDataURL(files[0])
    reader.onloadend = () => {
      this.setState({
        cover: URL.createObjectURL(files[0]),
        coverData: reader.result
      })
    }
  }

  /**
   * Удаляем картинку
   */
  removeImage() {
    this.setState({
      coverData: null
    })
  }

  /**
   * Сохраняем форму
   * @param values
   * @param form
   */
  onSubmit(values, form) {
    if (this.state.authors.length === 0) {
      // Валидируем по количеству авторов
      this.setState({
        authorError: ['Добавьте хотя бы одного автора']
      })
    } else {
      const book = {
        ...values,
        authors: this.state.authors,
        coverData: this.state.coverData
      }

      if (this.props.book) {
        // Сохраняем при редактировании
        book.id = this.props.book.id
        this.props.saveBook(book)
        history.push('/')
      } else {
        // Добавляем книгу
        this.props.addBook(book)
        form.reset()
        this.setState({
          authors: [],
          newAuthor: '',
          cover: null,
          authorError: []
        })
      }
    }
  }

  /**
   * Валидация обязательного поля
   * @param value - значение поля
   * @returns {boolean}
   */
  validateRequired(value) {
    if (value !== undefined) {
      value = value.trim()
    }
    return value !== '' && value !== undefined
  }

  /**
   * Валидация по длине
   * @param value - значение поля
   * @param length - длина поля
   * @returns {boolean}
   */
  validateLength(value, length) {
    return value.length <= length
  }

  render() {
    const {
      authors,
      coverData,
      authorError,
      newAuthor,
      showMessage
    } = this.state
    const { book } = this.props

    return (
      <div className="center">
        <Form
          onSubmit={this.onSubmit}
          validate={values => {
            // Валидация полей
            const errors = {}
            if (!this.validateRequired(values.title)) {
              errors.title = 'Введите заголовок'
            } else if (!this.validateLength(values.title, 30)) {
              errors.title = 'Длина заголовка не должна превышать 30 символов'
            }

            if (!this.validateRequired(values.pageCount)) {
              errors.pageCount = 'Введите количество страниц'
            } else {
              const pageCount = parseInt(values.pageCount)
              if (pageCount <= 0 || pageCount > 10000) {
                errors.pageCount =
                  'Количество страниц должно быть более 0 и менее 10 000'
              }
            }
            if (
              values.publisher &&
              !this.validateLength(values.publisher, 30)
            ) {
              errors.publisher =
                'Длина названия издательства не должна превышать 30 символов'
            }

            if (values.year) {
              const year = parseInt(values.year)
              const date = new Date()
              if (year < 1800) {
                errors.year = 'Год публикации не должен быть меньше 1800 года'
              } else if (year > date.getFullYear()) {
                errors.year = 'Год не должен превышать текущий год'
              }
            }

            if (values.date) {
              const date = new Date(values.date)
              const min = new Date('1800-01-01')

              if (date.getTime() < min.getTime()) {
                errors.date =
                  'Дата выхода в тираж не должна быть меньше 01.01.1800'
              }
            }

            if (values.isbn) {
              if (ISBN.parse(values.isbn) === null) {
                errors.isbn = 'Неверный формат ISBN'
              }
            }
            return errors
          }}
          render={({ handleSubmit, pristine, invalid }) => (
            <form className="form" onSubmit={handleSubmit} noValidate>
              <div className="form__input">
                <label className="form__label">Заголовок</label>
                <Field
                  name="title"
                  defaultValue={book ? book.title : ''}
                  render={({ input, meta }) => (
                    <div className="form__input__wrap">
                      <input
                        {...input}
                        type="text"
                        className={meta.error && meta.touched ? 'error' : ''}
                      />
                      {meta.error && meta.touched && (
                        <div className="form__error">{meta.error}</div>
                      )}
                    </div>
                  )}
                />
              </div>
              <div className="form__input">
                <label className="form__label">Имя и фамилия автора</label>
                <SuggestAdapter
                  name="author"
                  onChangeValue={this.onChangeAuthor}
                  renderFunc={suggestion => {
                    return `${suggestion.firstName} ${suggestion.lastName}`
                  }}
                  suggestions={authorList}
                  placeholder={'Имя Фамилия'}
                  reset={newAuthor === ''}
                />
                {authorError.length > 0 && (
                  <ul className="form__error-list">
                    {authorError.map(err => (
                      <li className="form__error" key={err}>
                        {err}
                      </li>
                    ))}
                  </ul>
                )}
                <a className="form__btn" onClick={this.addAuthor}>
                  Добавить
                </a>
              </div>
              {authors.length > 0 && (
                <div className="form__list">
                  <div className="form__list__title">Авторы</div>
                  <div className="form__list__items">
                    {authors.map(author => (
                      <div className="form__list__item" key={author}>
                        {author}
                        <div
                          className="form__list__del cross"
                          onClick={e => this.removeAuthor(author)}
                        />{' '}
                      </div>
                    ))}
                  </div>
                </div>
              )}
              <div className="form__input">
                <label className="form__label">Количество страниц</label>
                <Field
                  name="pageCount"
                  defaultValue={book ? book.pageCount : ''}
                  render={({ input, meta }) => (
                    <div className="form__input__wrap">
                      <input
                        {...input}
                        type="number"
                        className={meta.error && meta.touched ? 'error' : ''}
                      />
                      {meta.error && meta.touched && (
                        <div className="form__error">{meta.error}</div>
                      )}
                    </div>
                  )}
                />
              </div>
              <div className="form__input">
                <label className="form__label">Издательство</label>
                <Field
                  name="publisher"
                  component={SuggestAdapter}
                  renderFunc={suggestion => {
                    return suggestion
                  }}
                  suggestions={publisherList}
                  placeholder={''}
                  defaultValue={book ? book.publisher : ''}
                />
              </div>
              <div className="form__input">
                <label className="form__label">Год публикации</label>
                <Field
                  name="year"
                  defaultValue={book ? book.year : ''}
                  render={({ input, meta }) => (
                    <div className="form__input__wrap">
                      <input
                        {...input}
                        type="number"
                        className={meta.error && meta.touched ? 'error' : ''}
                      />
                      {meta.error && meta.touched && (
                        <div className="form__error">{meta.error}</div>
                      )}
                    </div>
                  )}
                />
              </div>
              <div className="form__input">
                <label className="form__label">Дата выхода в тираж</label>
                <Field
                  name="date"
                  defaultValue={book ? book.date : ''}
                  render={({ input, meta }) => (
                    <div className="form__input__wrap">
                      <input
                        {...input}
                        type="date"
                        className={meta.error && meta.touched ? 'error' : ''}
                      />
                      {meta.error && meta.touched && (
                        <div className="form__error">{meta.error}</div>
                      )}
                    </div>
                  )}
                />
              </div>
              <div className="form__input">
                <label className="form__label">ISBN</label>
                <Field
                  name="isbn"
                  defaultValue={book ? book.isbn : ''}
                  render={({ input, meta }) => (
                    <div className="form__input__wrap">
                      <input
                        {...input}
                        type="text"
                        className={meta.error && meta.touched ? 'error' : ''}
                      />
                      {meta.error && meta.touched && (
                        <div className="form__error">{meta.error}</div>
                      )}
                    </div>
                  )}
                />
              </div>
              <div className="form__input">
                <label className="form__label">Обложка</label>
                {coverData === null && (
                  <div className="form__file">
                    <input
                      type="file"
                      accept="image/jpeg,image/png,image/gif"
                      onChange={this.selectImage}
                    />
                    <div className="form__file__label">Загрузите обложку</div>
                  </div>
                )}
                {coverData !== null && (
                  <div className="form__image">
                    <div className="form__image__wrap">
                      <div
                        className="form__image__del cross"
                        onClick={this.removeImage}
                      />
                      <img src={coverData} className="form__image__img" />
                    </div>
                  </div>
                )}
              </div>
              <button className="form__btn" type="submit">
                Сохранить книгу
              </button>
            </form>
          )}
        />
        <div className={`popup${showMessage ? ' popup_visible' : ''}`}>
          Книга сохранена
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const {
    book: { books }
  } = state
  return {
    bookCount: books.length,
    book: books.find(b => b.id === ownProps.match.params.uuid)
  }
}

export default connect(
  mapStateToProps,
  { addBook, saveBook }
)(BookForm)
