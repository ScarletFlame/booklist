/*
Компонент заголовка
 */
import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
      <div className="header">
        <div className="center">Редактор книг</div>
      </div>
    )
  }
}
