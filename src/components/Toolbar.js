/**
 * Компонент меню
 */
import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

export default class Toolbar extends Component {
  render() {
    return (
      <div className="toolbar">
        <div className="center">
          <NavLink exact={true} to="/" className="toolbar__link">
            Список книг
          </NavLink>
          <NavLink to="/add" className="toolbar__link">
            Добавить книгу
          </NavLink>
        </div>
      </div>
    )
  }
}
