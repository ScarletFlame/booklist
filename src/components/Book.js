/*
Компонент отдельной книги
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import moment from 'moment'
import { connect } from 'react-redux'
import { deleteBook } from '../actions/book'

class Book extends Component {
  static propTypes = {
    title: PropTypes.string,
    authors: PropTypes.arrayOf(PropTypes.string),
    date: PropTypes.string,
    publisher: PropTypes.string,
    isbn: PropTypes.string,
    year: PropTypes.string,
    pageCount: PropTypes.string,
    coverData: PropTypes.string,
    id: PropTypes.string,
    deleteBook: PropTypes.func
  }

  render() {
    // Выводим дату красиво
    const date = this.props.date ? moment(this.props.date) : null

    return (
      <div className="book">
        {this.props.coverData && (
          <div className="book__image">
            <img src={this.props.coverData} className="book__img" />
          </div>
        )}

        <div className="book__info">
          <div className="book__title">{this.props.title}</div>
          <div className="book__row">
            <div className="book__author">{this.props.authors.join(', ')}</div>
            {this.props.year && (
              <div className="book__year">, {this.props.year}</div>
            )}
          </div>
          {this.props.publisher && (
            <div className="book__row">
              <div className="book__publisher">{this.props.publisher}</div>
              {date && (
                <div className="book__date">
                  , {date.locale('ru').format('DD MMMM YYYY')}
                </div>
              )}
              {this.props.pageCount && (
                <div className="book__count">
                  &mdash; {this.props.pageCount} с.
                </div>
              )}
            </div>
          )}
          {this.props.isbn && (
            <div className="book__row">
              <div className="book__publisher">ISBN: {this.props.isbn}</div>
            </div>
          )}
          <div className="book__links">
            <NavLink to={`/edit/${this.props.id}`} className="book__link">
              Редактировать
            </NavLink>
            <a
              className="book__link"
              onClick={e => this.props.deleteBook(this.props.id)}
            >
              Удалить
            </a>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  null,
  { deleteBook }
)(Book)
