/*
Адаптер автоподстановки
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Autosuggest from 'react-autosuggest'

/**
 * Удаляем из строки символы
 * @param str
 * @returns {*}
 */
const escapeRegexCharacters = str => {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
}

/**
 * Получаем список подстановкм по заданному значению
 * @param value - введенная строка
 * @param suggestions - список подстановок
 * @param getValue - функция, которая формирует конечную строку
 * @returns {Array|*}
 */
const getSuggestions = (value, suggestions, getValue) => {
  const escapedValue = escapeRegexCharacters(value.trim())
  if (escapedValue === '') {
    return []
  }

  const regex = new RegExp('^' + escapedValue, 'i')

  return suggestions.filter(suggestion => regex.test(getValue(suggestion)))
}

/**
 * Формируем значение для вывода
 * @param suggestion - объект подстановки
 * @param getValue - функция, которая формирует конечную строку
 * @returns {*}
 */
const renderSuggestion = (suggestion, getValue) => {
  return <span>{getValue(suggestion)}</span>
}

export default class SuggestAdapter extends Component {
  static propTypes = {
    input: PropTypes.shape({
      onChange: PropTypes.func,
      value: PropTypes.any
    }),
    meta: PropTypes.shape({
      error: PropTypes.string
    }),
    onChangeValue: PropTypes.func,
    suggestions: PropTypes.array,
    renderFunc: PropTypes.func,
    placeholder: PropTypes.string,
    reset: PropTypes.bool
  }

  constructor(props) {
    super(props)

    this.state = {
      value: '',
      suggestions: []
    }

    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(
      this
    )
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(
      this
    )
    this.onChange = this.onChange.bind(this)
  }

  componentDidUpdate(prevProps) {
    // Очищаем поле при воздействии извне
    if (prevProps.reset === false && this.props.reset === true) {
      this.setState({
        value: ''
      })
    }
  }

  /**
   * Изменение значения поля
   * @param newValue - новое значение поля
   */
  onChange({ newValue }) {
    this.setState({
      value: newValue
    })
  }

  /**
   * ПОказываем подстановки
   * @param value - введенное значение
   */
  onSuggestionsFetchRequested({ value }) {
    this.setState({
      suggestions: getSuggestions(
        value,
        this.props.suggestions,
        this.props.renderFunc
      )
    })
  }

  onSuggestionsClearRequested() {
    this.setState({
      suggestions: []
    })
  }

  render() {
    const { value, suggestions } = this.state
    const { input, meta, onChangeValue, renderFunc, placeholder } = this.props

    const initialValue =
      input && input.hasOwnProperty('value') ? input.value : value

    const inputProps = {
      placeholder: placeholder,
      value: initialValue,
      onChange: (e, value) => {
        this.onChange(value)
        if (onChangeValue) {
          onChangeValue(value.newValue)
        }
        if (input) {
          input.onChange(value.newValue)
        }
      }
    }

    return (
      <div className="form__input__wrap">
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={renderFunc}
          renderSuggestion={suggestion =>
            renderSuggestion(suggestion, renderFunc)
          }
          inputProps={inputProps}
        />
        {meta && meta.error && <div className="form__error">{meta.error}</div>}
      </div>
    )
  }
}
