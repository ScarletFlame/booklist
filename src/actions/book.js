/*
Дейсвия redux для управления книгами
 */

/**
 * Добавляем книгу
 * @param book - книга
 * @returns {{payload: {book: *}, type: string}}
 */
export const addBook = book => ({
  type: 'ADD_BOOK',
  payload: {
    book
  }
})

/**
 * Загружаем книги из localStorage
 * @param books - книги
 * @returns {{payload: {books: *}, type: string}}
 */
export const loadBooks = books => ({
  type: 'LOAD_BOOKS',
  payload: {
    books
  }
})

/**
 * Удаляем книгу по ее id
 * @param uuid - айди книги
 * @returns {{payload: {uuid: *}, type: string}}
 */
export const deleteBook = uuid => ({
  type: 'DELETE_BOOK',
  payload: {
    uuid
  }
})

/**
 * Сохраняем книгу
 * @param book - книга
 * @returns {{payload: {book: *}, type: string}}
 */
export const saveBook = book => ({
  type: 'SAVE_BOOK',
  payload: {
    book
  }
})

/**
 * Сортировка книг
 * @param field - поле, по которому сортирубт
 * @param dir - направление сортировки
 * @returns {{payload: {field: *, dir: *}, type: string}}
 */
export const sortBooks = (field, dir) => ({
  type: 'SORT_BOOKS',
  payload: {
    field,
    dir
  }
})
